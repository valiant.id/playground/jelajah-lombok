import type { RequestHandler } from '@sveltejs/kit';

function config_to_array(config: string, separator = ',') {
	return config.split(separator).map((v) => v.trim());
}

export const GET: RequestHandler = async ({ url }) => {
	const { VITE_SITES, VITE_VIDEOS } = import.meta.env;

	if (!VITE_SITES || !VITE_VIDEOS) {
		throw new Error('Invalid configuration.');
	}

	const videos = config_to_array(VITE_VIDEOS);
	const video_src = videos[Math.floor(Math.random() * videos.length)];
	const sites = config_to_array(VITE_SITES).map((s) => config_to_array(s, '|'));
	const { hostname } = url;
	const site = sites.find(([domain]) => hostname === domain) || sites[0];

	return {
		body: {
			video_src,
			description: site[2],
			title: site[1],
		},
	};
};
