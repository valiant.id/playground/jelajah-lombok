# Fullscreen Video

A placeholder site showing fullscreen video built with [SvelteKit](https://kit.svelte.dev/).

## Configuration

Copy `.env.example` to `.env` then edit it to your liking.

- `VITE_SITES` should contain comma-separated list of site configs formatted as `<domain>|<title>|<description>`
- `VITE_VIDEOS` should contain comma-separated list of video URLs.

## Developing

```bash
pnpm install
pnpm dev
```

## Building

```bash
pnpm build
```

## Deploying

Make sure to add the environment variables to your host of choice.
